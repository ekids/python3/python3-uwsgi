#! /usr/bin/env python3

from pprint import pformat
import html


def application(environ, start_response):
    # show the environment:
    environ_escaped = html.escape(pformat(environ), quote=False)
    output = ['<pre>']
    output.append(environ_escaped)
    output.append('</pre>')

    # Create a simple form:
    output.append('<form method="post">')
    output.append('<input type="text" name="test">')
    output.append('<input type="submit">')
    output.append('</form>')

    if environ['REQUEST_METHOD'] == 'POST':
        # Show form data as received by POST:
        output.append('<h1>FORM DATA</h1>')
        output.append(pformat(environ['wsgi.input'].read()))

    # Send results
    output_len = sum(len(line) for line in output)
    start_response('200 OK', [('Content-type', 'text/html; charset=utf-8'),
                              ('Content-Length', str(output_len))])
    # Convert whole output array to utf-8:
    output_encoded = (line.encode('utf-8') for line in output)
    return output_encoded
